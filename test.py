#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
from PyQt5.QtWidgets import *

from PyQt5.QtCore import *
from PyQt5.QtGui import *

class MainWindow(QWidget, object):

  def __init__(self, parent=None):

    super(MainWindow, self).__init__(parent)

    self.setWindowTitle('A sample app')

    self.button = QPushButton("Click me!", self)
    self.button.clicked.connect(self.after_click)

    self.textEdit = QTextEdit()
    self.files = QFileDialog()


    self.mainLayout = QHBoxLayout(self)
    self.leftLayout = QVBoxLayout()
    self.leftLayout.addWidget(self.button)
    self.leftLayout.addWidget(self.textEdit)
    self.rightLayout = QVBoxLayout()

    #self.rightLayout.addWidget(self.files)
    
    self.mainLayout.addLayout(self.leftLayout, 3)
    self.mainLayout.addLayout(self.rightLayout, 3)

    self.files.setFileMode(QFileDialog.AnyFile)
   # self.setFilter("Text files (*.txt)")
   
    
    


  def after_click(self):
    fileName = self.files.getOpenFileName(self)[0]
    f = open(fileName,'r')
    data = f.read()
    self.textEdit.setText(data)
    # text = self.textEdit.toPlainText()
     # file.write(text)
     # file.close()
  # with open('./log.txt','w') as f:
  # f.write(str(self.textEdit.toPlainText()))



def main():
  app = QApplication(sys.argv)
  w = MainWindow()
  w.show()
  return app.exec()

if __name__ == '__main__':
  main()
